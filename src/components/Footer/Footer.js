import React from 'react';

const Footer = () => {
    return (
        <footer className="py-5 bg-dark">
            <div className="container">
                <p className="m-0 text-center text-white"><h2>It Is Footer</h2></p>
            </div>
        </footer>
    );
};

export default Footer;